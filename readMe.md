# Algorithme de recalage ICP

Cet algorithme permet de faire un recalage sur un nuage de point.

## Installation

clone the repo

installation de [ANN](https://www.cs.umd.edu/~mount/ANN/)

compilation de [ANN](https://www.cs.umd.edu/~mount/ANN/Files/1.1.2/ANNmanual_1.1.pdf)


## Usage

Une fois ANN installé, il est possible de compiler avec la commande suivante
```
g++ basic_icp_registration.cpp Matrix.cpp UtilMaths.cpp Vector.cpp -o exec -Iann/include -Lann/lib -lANN

./exec
```

### Data

Les données sont un fichier texte avec les coordonnées des points en 3 dimensions

## Autors

### Auteur code source : Taha RIDENE
### Auteurs rendu : Sylvain LEJAMBLE - Antoine MALINET