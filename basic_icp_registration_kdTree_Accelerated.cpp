#include <stdio.h>
#include "Matrix.h"

#include <ANN/ANN.h> // ANN declarations

typedef float *TabFloat;

using namespace std;

// Definition d'une structure specifique pour stocker les voisins
struct Voisin{
	int index;
	float distance;
};

int main(int argc, char* argv[])
{
	// initialisation des fichiers
	FILE* in1 = fopen("../../../../data/3D.samples/bunny/bunny.pts.neu", "r"); 
	FILE* in2 = fopen("../../../../data/3D.samples/bunny/bunny_perturbed.pts.neu", "r");

	FILE* out1 = fopen("../../../../data/3D.samples/bunny/result.neu", "w");
	FILE* out2 = fopen("../../../../data/3D.samples/bunny/error.txt", "w");

	FILE* out3 = fopen("../../../../data/3D.samples/bunny/transformation.txt", "w");
	FILE* out4 = fopen("../../../../data/3D.samples/bunny/tempsdexecution.txt", "w");
	// ******************************************************************
	/* Variables : compteurs*/
	int i, j, iteration = 0;
	float x, y, z;
	int n = 3, NbrPt1 = 0, NbrPt2 = 0;

	/* Variables de temps d'ex�cution*/
	//LARGE_INTEGER start, end, freq;

	/*tableaux de points*/
	//TabFloat* pointRef, * pointCible, * Voisin;
	TabFloat* pointRef, * pointCible;
	/*Tableau de voisins*/
	Voisin* voisin;
	//-- Nos variables --
	//transformation T(R,t)
	Matrix R(n, n, false); // Matrice de rotation 
	Vector t(3); //vecteur de translation 

	//Calcul de la SVD {H.ComputeSVD(U,W,V);}
	Matrix H(n, n, false); //Matrice 	
	Matrix U(3, 3, false);
	Matrix W(3, 3, false);
	Matrix V(3, 3, false);

	int NbrPtC, NbrPtV; // nombre des points des fichiers
	float seuil; // le seuil � utilis� par ICP 
	float erreur = 1000; // l'�cart type moyen (~1000) valeur par d�faut
	float max; //utilis� pour la distance maximale entre 2 points (distance cart�sienne)
	float min; //utilis�e pour la distance minimale entre 2 points (distance cart�sienne)
	int k; // compteur des it�ration d'ICP
	int l; // indices utilisable pour la programmation

	Vector pm1(3); //centre de gravit� nuage 1
	Vector pm2(3); //centre de gravit� nuage 2

	// *********************** INITIALISATION *******************************************
	
	//-- Init : nombre points : lecture pour comptage des points 
	while (fscanf(in1, "%f %f %f", &x, &y, &z) != EOF)
		NbrPt1++;
	while (fscanf(in2, "%f %f %f", &x, &y, &z) != EOF)
		NbrPt2++;
	//-- Init allocation memoire :  allocations des tableaux des deux ensembles de points
	pointRef = new TabFloat[NbrPt1];
	for (i = 0; i < NbrPt1; i++)
		pointRef[i] = new float[3];

	pointCible = new TabFloat[NbrPt2];
	for (i = 0; i < NbrPt2; i++)
		pointCible[i] = new float[3];

	//Init : retour au d�but du fichier
	rewind(in1);
	rewind(in2);


	ANNpointArray arrayRef = new ANNpoint[NbrPt1];
	for (i = 0; i < NbrPt1; i++)
		arrayRef[i] = annAllocPt(3);
	
	
	ANNpointArray arrayCible = new ANNpoint[NbrPt2];
	for (i = 0; i < NbrPt2; i++)
		arrayCible[i] = annAllocPt(3);
	
	
	ANNdistArray distances = new ANNdist[NbrPt1];
	for (i = 0; i < NbrPt1; i++)
		arrayRef[i] = annAllocPt(3);
	
	
	ANNidxArray idxArray = new ANNidx[NbrPt1];
	//Init :  tableau du plus proche voisin
	voisin = new Voisin[NbrPt1];
	//Init :  lecture pour la mise m�moire	
	for (i = 0; i < NbrPt1; i++) {
		fscanf(in1, "%f %f %f", &pointRef[i][0], &pointRef[i][1], &pointRef[i][2]);
		
		arrayRef[i][0] = pointRef[i][0];
		arrayRef[i][1] = pointRef[i][1];
		arrayRef[i][2] = pointRef[i][2];
		
		voisin[i].index = i;
		voisin[i].distance = 0;
		//printf("%f", arrayRef[i][0]);
	}
	for (i = 0; i < NbrPt2; i++) {
		fscanf(in2, "%f %f %f", &pointCible[i][0], &pointCible[i][1], &pointCible[i][2]);
		arrayCible[i][0] = pointCible[i][0];
		arrayCible[i][1] = pointCible[i][1];
		arrayCible[i][2] = pointCible[i][2];
	}

	NbrPtC = NbrPt2;
	k = 0, l = 0;
	
	ANNkd_tree tree(arrayCible, NbrPt2, 3); // Creation du KDTree
		
	// ************************** ITERATION ICP ****************************************	
	// Boucle d'iteration selon l'erreur ou/et le nombre d'it�ration
	while ((erreur > 0.1) && (k < 100))
	{

		/* D�but du temps d'ex�cution de l'it�ration*/
		//QueryPerformanceFrequency(&freq);
		/* Lancement de la mesure     */
		//QueryPerformanceCounter(&start);
		/* Affichage du compteur K    */
		printf("L'iteration %d \n", k); k++;
		//*************************************************************************************
		//*** STEP 0 : Calcul du seuil en fonction de la distance max oubien fixation en dure
		//*************************************************************************************
		max = sqrt(annDist(3, arrayRef[0], arrayCible[0]));
		for (i = 0; i < NbrPt1; i++)
		{
			for (j = 1; j < NbrPt2; j++)
			{
				if (max < sqrt(annDist(3, arrayRef[i], arrayCible[j])))
					max = sqrt(annDist(3, arrayRef[i], arrayCible[j]));
			}
		}
		seuil = 250;
		printf("Max: %f\n", max);
		//*************************************************************************************
		//*** STEP 1 : Recherche des plus proches voisins : TO DO
		//*************************************************************************************
		for (i = 0; i < NbrPt1; i++)
		{
			tree.annkSearch(arrayRef[i],
							1, 
							idxArray, 
							distances);
							
			//remove outsiders
			if (distances[0] > seuil) {
				voisin[i].index = -1;
			}
			else {
				voisin[i].index = idxArray[0];
				voisin[i].distance = sqrt(distances[0]);
			}
		}
		//*************************************************************************************
		//*** STEP 2: CALCUL DE LA TRANSFORMEE : TO DO
		//*************************************************************************************
		//-- STEP 2.1 : Recherche des centres de masse : TO DO	
		Vector pm1(3);
		Vector pm2(3);
		int N = 0;
		Vector somme1(3);
		Vector somme2(3);
		for (i = 0; i < NbrPt1; i++)
		{
			if (voisin[i].index != -1) {
				N++;
				pointRef[i][0] = arrayRef[i][0];
				pointRef[i][1] = arrayRef[i][1];
				pointRef[i][2] = arrayRef[i][2];
				somme1.operator +=(pointRef[i]);
				l = voisin[i].index;
				pointCible[l][0] = arrayCible[l][0];
				pointCible[l][1] = arrayCible[l][1];
				pointCible[l][2] = arrayCible[l][2];
				somme2.operator += (pointCible[l]);
			}
		}

		pm1 = somme1 / N;
		pm2 = somme2 / N;

		printf("Centre de masse 1 : (%f, %f, %f)\n", pm1[0], pm1[1], pm1[2]);
		printf("Centre de masse 2 : (%f, %f, %f)\n", pm2[0], pm2[1], pm2[2]);
		
		
		Vector q1(3);
		Vector q2(3);
		//-- STEP 2.2 : Calcul de H
		H.SetToZero();
		for (i = 0; i < NbrPt1; i++)
		{
			//if (Voisin[i][1] != -1)
			if (voisin[i].index != -1)
			{
				q1[0] = arrayRef[i][0] - pm1[0];
				q1[1] = arrayRef[i][1] - pm1[1];
				q1[2] = arrayRef[i][2] - pm1[2];

				l = voisin[i].index;

				q2[0] = arrayCible[l][0] - pm2[0];
				q2[1] = arrayCible[l][1] - pm2[1];
				q2[2] = arrayCible[l][2] - pm2[2];
				H = H + abT(q2, q1);
			}
		}
		//-- STEP 2.3 : Estimation de la transformation rigide (R,t)
		H.ComputeSVD(U, W, V);
		R = V * Transpose(U);
		Vector t(3);
		t = pm1 - R * pm2;
		//*************************************************************************************
		//*** STEP 3: APPLICATION DE LA TRANSFORMEE : TO DO
		//*************************************************************************************
		//--STEP 3.1 Application de la transformation :	
		Vector tampon(3);
		Vector tempPoint2(3);

		for (i = 0; i < NbrPt2; i++)
		{
			tempPoint2[0] = arrayCible[i][0];
			tempPoint2[1] = arrayCible[i][1];
			tempPoint2[2] = arrayCible[i][2];

			tampon = R * tempPoint2;

			for (j = 0; j < 3; j++)
				arrayCible[i][j] = tampon[j] + t[j];
		}
		//--STEP 3.2 fin de temps d'ex�cution 

		/* Arret de la mesure         */
		//QueryPerformanceCounter(&end);
		/* Conversion en millsecondes */
		//elapsed = (1000.0 * (end.QuadPart - start.QuadPart)) / freq.QuadPart;
		//printf("Tps execution iteraration : %d : %.0f millisecondes entre start et end.\n", k, elapsed);
		//fprintf(out4, "%.0f\n", elapsed);

		//*************************************************************************************
		//*** STEP 4: CALCUL DE L'ERREUR ET AFFICHAGE DES RESULTATS : TO DO
		//*************************************************************************************
		erreur = 0;
		for (i = 0; i < NbrPt1; i++)
		{
			if (voisin[i].index != -1)
			{
				l = voisin[i].index;
				erreur = erreur + sqrt(annDist(3, arrayRef[i], arrayCible[l]));
			}
		}
		erreur = erreur / N;
		fprintf(out2, "%f \n", erreur);
		fprintf(out3, "It�ration %d\nT : %f %f %f\n", k, t[0], t[1], t[2]);
		fprintf(out3, "R : \n");

		for (i = 0; i < 3; i++)
			fprintf(out3, "%f %f %f\n\n", R(i, 0), R(i, 1), R(i, 2));
		
		//queryStream.close();
	
		printf("erreure: (%f)\n", erreur);
	}//fin boucle while
	// ******************************************************************

	// sauvegarde du fichier
	printf("Sauvegarde du fichier\n");
	for (i = 0; i < NbrPt2; i++)
		fprintf(out1, "%f %f %f\n", arrayCible[i][0], arrayCible[i][1], arrayCible[i][2]);

	printf("Sauvegarde du fichier\n");

	for (i = 0; i < NbrPt2; i++)
		delete[] pointCible[i];
	
	for (i = 0; i < NbrPt2; i++)
		delete[] pointRef[i];
	
	delete[] pointRef;
	delete[] pointCible;
	delete[] voisin;
	
	for (i = 0; i < NbrPt1; i++)
		annDeallocPt(arrayRef[i]);
	
	delete[] arrayRef;
	
	for (i = 0; i < NbrPt2; i++)
		annDeallocPt(arrayCible[i]);
	
	delete[] arrayCible;
	delete[] distances;
	delete[] idxArray;

	
	fclose(in1);
	fclose(in2);
	fclose(out1);
	fclose(out2);
   	fclose(out3);
	fclose(out4);
	return 0;
}